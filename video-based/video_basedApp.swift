//
//  video_basedApp.swift
//  video-based
//
//  Created by Kitti Jarearnsuk on 13/9/2565 BE.
//

import SwiftUI

@main
struct video_basedApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
